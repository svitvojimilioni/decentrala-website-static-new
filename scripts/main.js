const theme_switcher = document.getElementById("theme-switcher");
const imgs = document.getElementsByTagName("img");
const sections_button = document.getElementById("sections-button");
const sections_menu = document.getElementById("sections-menu");
const main = document.getElementById("main");

let theme = window.localStorage.getItem("theme");

if (theme !== null) {
  if (theme === "light") {
    changeToLightTheme();
  } else {
    changeToDarkTheme();
  }
} else {
  if (
    window.matchMedia &&
    window.matchMedia("(prefers-color-scheme: dark)").matches
  ) {
    changeToDarkTheme();
  }
}

theme_switcher.addEventListener("click", () => {
  if (theme_switcher.textContent.indexOf("off") !== -1) {
    changeToDarkTheme();
  } else {
    changeToLightTheme();
  }
});

function changeToDarkTheme() {
  theme_switcher.textContent = "turn the light on";
  document.documentElement.style.setProperty("--border", "var(--dark-border)");
  document.documentElement.style.setProperty("--text", "var(--dark-text)");
  document.documentElement.style.setProperty("--bg", "var(--dark-bg)");
  window.localStorage.setItem("theme", "dark");
  for (let i = 0; i < imgs.length; i += 1) {
    imgs[i].src = imgs[i].src.replace("-light", "-dark");
  }
}

function changeToLightTheme() {
  theme_switcher.textContent = "turn the light off";
  document.documentElement.style.setProperty("--border", "var(--light-border)");
  document.documentElement.style.setProperty("--text", "var(--light-text)");
  document.documentElement.style.setProperty("--bg", "var(--light-bg)");
  window.localStorage.setItem("theme", "light");
  for (let i = 0; i < imgs.length; i += 1) {
    imgs[i].src = imgs[i].src.replace("-dark", "-light");
  }
}

function closeMenu() {
  sections_button.setAttribute("opened", "false");
  sections_button.children[0].src = sections_button.children[0].src.replace(
    "opened",
    "closed",
  );
  sections_menu.style.display = "none";
}

function openMenu() {
  sections_button.setAttribute("opened", "true");
  sections_button.children[0].src = sections_button.children[0].src.replace(
    "closed",
    "opened",
  );
  sections_menu.style.display = "flex";
  sections_menu.style.flexDirection = "column";
}

sections_button.addEventListener("click", () => {
  if (sections_button.getAttribute("opened") === "false") {
    openMenu();
  } else {
    closeMenu();
  }
});

window.addEventListener("resize", () => {
  if (sections_button.getAttribute("opened") === "true") {
    closeMenu();
  }
});

main.addEventListener("click", (event) => {
  if (sections_button.getAttribute("opened") === "true") {
    closeMenu();
  }
});

DEBUG;
console.log(window.innerWidth);
console.log(window.innerHeight);
