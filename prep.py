#! /usr/bin/env python

from datetime import datetime
from functools import cmp_to_key

days = [
    "Ponedeljak",
    "Utorak",
    "Sreda",
    "Cetvrtak",
    "Petak",
    "Subota",
    "Nedelja",
]

months = [
    "Januar",
    "Februar",
    "Mart",
    "April",
    "Maj",
    "Jun",
    "Jul",
    "Avgust",
    "Septembar",
    "Oktobar",
    "Novembar",
    "Decembar",
]

today = datetime.today().date()

def parse_date(date):
    return datetime.strptime(date,"%d-%m-%Y").date()

def compare_events(one, two):
    one = parse_date(one.split(", ")[0])
    two = parse_date(two.split(", ")[0])
    if one>two:
        return 1
    elif one==two:
        return 0
    else:
        return -1

def is_past_event(event):
    return event < today

def load_events():
    events = []
    with open("dogadjaji.txt", "rt") as file:
        file.readline()
        for event in file.readlines():
            event = event.strip()
            if event != "":
                events.append(event)
    return events

def write_events(events):
    with open("dogadjaji.txt", "wt") as file:
        file.write("datum, vreme, lokacija, tema\n")
        for event in events:
            file.write(event+"\n")

def sort_events(events):
    return sorted(events, key = cmp_to_key(compare_events))


events = load_events()
events = sort_events(events)
write_events(events)

start = "<table>\n<tr>\n<th>Datum</th>\n<th>Vreme</th>\n<th>Mesto</th>\n<th>Tema</th>\n</tr>\n"
end="\n</table>"
future_events = []

for event in events:
    date, time, location, title = event.split(", ")
    date = parse_date(date)
    if is_past_event(date):
        continue
    date = days[date.weekday()]+", "+str(date.day)+". "+months[date.month-1]+" "+str(date.year)+"."
    time = time+"h"
    future_event = []
    future_event.append("<td> "+date+" </td>")
    future_event.append("<td> "+time+" </td>")
    if "https://" in location:
        place,link = location.split("https://")
        future_event.append("<td> <a href=\"https://"+link+"\""+"> "+place.strip()+" </a> </td>")
    else:
        future_event.append("<td> "+location.strip()+" </td>")
    future_event.append("<td> "+title+" </td>")
    future_events.append("<tr>\n"+"\n".join(future_event)+"\n</tr>")

events = []
events.append(start)

for event in future_events:
    events.append(event)

events.append(end)

with open("pages/dogadjaji.html","rt") as file:
    html = file.readlines()

new_html = []

i=0
while i<len(html):
    original_line = html[i]
    line = original_line.strip()
    if line == '<!-- dogadjaji start -->':
        new_html.append(original_line)
        for event in events:
            new_html.append(event+"\n")
        new_html.append("    <!-- dogadjaji end -->\n")
        while line!='<!-- dogadjaji end -->':
            i+=1
            line=html[i].strip()
        i+=1
        while i<len(html):
            new_html.append(html[i])
            i+=1
    else:
        new_html.append(original_line)
        i+=1


with open("pages/dogadjaji.html","wt") as file:
    file.writelines(new_html)