# Decentrala

Redisign of dmz.rs .

# STILL NOT COMPLETED

### TODO:

- [x] create themes switcher
  - [x] "demo"
  - [x] propagate to all pages
  - [x] store theme to localStorage
  - [x] read "user agent" for default theme
  - [x] invert images and icons
- update projects section
- create blogging system
- create xmpp bot that connects to events section.
- [ ] make responsive
  - create menus for smaller screens
    - [x] created one menu
      - this might be enough
  - [x] adjust the mesh depending on the screen size
    - no mesh on small screens
  - tweak other random issues with layout
- make webring system
- make english version (localisation)

<!-- there is no place like ~/home -->
